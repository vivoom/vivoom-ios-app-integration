//
//  AppDelegate.h
//  VivoomWebIntegrationApp
//
//  Created by dev on 8/24/16.
//  Copyright © 2016 Vivoom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) UINavigationController *navigationController;

@end

