//
//  ViewController.h
//  VivoomWebIntegrationApp
//
//  Created by dev on 8/24/16.
//  Copyright © 2016 Vivoom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate, WKNavigationDelegate, WKUIDelegate>
{
}

@property (nonatomic, strong) UITextField *urlTextField;
@property (nonatomic, strong) UIView *webContainerView;
@property (nonatomic, strong) UIBarButtonItem *closeButtonItem;
@property (nonatomic, strong) WKWebViewConfiguration *webViewConfiguration;
@property (nonatomic, strong) WKWebView *vivoomWebView;
@property (nonatomic, strong) NSMutableURLRequest *urlRequest;


// Cookie Data
@property (nonatomic, strong) NSString *cookieDataString;
@property (nonatomic, strong) NSMutableArray *cookieDataArray;

@end

