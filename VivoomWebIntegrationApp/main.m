//
//  main.m
//  VivoomWebIntegrationApp
//
//  Created by William Gordon on 10/24/16.
//  Copyright © 2016 Vivoom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
