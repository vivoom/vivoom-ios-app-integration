//
//  ViewController.m
//  VivoomWebIntegrationApp
//
//  Created by dev on 8/24/16.
//  Copyright © 2016 Vivoom, Inc. All rights reserved.
//


#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize urlTextField, webContainerView, closeButtonItem, vivoomWebView, webViewConfiguration, urlRequest;
@synthesize cookieDataString, cookieDataArray;


- (void)loadView
{
    self.view = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.view.backgroundColor = [UIColor colorWithWhite:0.18 alpha:1.0];
    
    
    // This is important if you are loading cookie data from previous
    // user visits to the campaign.
    [self loadCookieData];
    
    
    self.title = @"WebView";
    
    [self buildUserInterface];
    
    self.closeButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(handleCloseButtonTouched:)];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)buildUserInterface
{
    // Using programmatic means to create and display the user interface for clarity
    // in documentation.
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    infoLabel.textAlignment = NSTextAlignmentLeft;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle3];
    infoLabel.text = @"Enter URL here:";
    infoLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:infoLabel];
    
    
    self.urlTextField = [[UITextField alloc] init];
    self.urlTextField.delegate = self;
    self.urlTextField.borderStyle = UITextBorderStyleNone;
    self.urlTextField.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    self.urlTextField.backgroundColor = [UIColor colorWithWhite:0.92 alpha:1.0];
    self.urlTextField.placeholder = @"https://demo.vivoom-your-URL.co";
    self.urlTextField.textColor = [UIColor blackColor];
    self.urlTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.urlTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.urlTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.urlTextField.keyboardType = UIKeyboardTypeURL;
    self.urlTextField.returnKeyType = UIReturnKeyDone;
    self.urlTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    self.urlTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.urlTextField.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.urlTextField];
    
    
    
    // For testing: Update with the URL to your campaign. Typically, this will
    // be provided to you and the user will not manually enter the URL.
    self.urlTextField.text = @"https://your-demo.vivoom.co";
    
    
    
    UIButton *goButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [goButton addTarget:self action:@selector(handleLaunchWebView:) forControlEvents:UIControlEventTouchUpInside];
    [goButton setTitle:@"LAUNCH" forState:UIControlStateNormal];
    [goButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    goButton.titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    goButton.backgroundColor = [UIColor grayColor];
    goButton.translatesAutoresizingMaskIntoConstraints = NO;
    goButton.layer.cornerRadius = 5.0;
    [self.view addSubview:goButton];
    
    
    NSDictionary *viewDict = NSDictionaryOfVariableBindings(infoLabel, urlTextField, goButton);
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20.0-[infoLabel]-20.0-|" options:0 metrics:nil views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20.0-[urlTextField]-20.0-|" options:0 metrics:nil views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50.0-[goButton]-50.0-|" options:0 metrics:nil views:viewDict]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50.0-[infoLabel]-16.0-[urlTextField(36.0)]-24.0-[goButton(48.0)]" options:0 metrics:nil views:viewDict]];
}


#pragma mark - Button Action


- (void)handleLaunchWebView:(id)sender
{
    NSLog(@"[ViewController] -handleLaunchWebView- -> YOUR CODE");
    
    // DEV NOTE #1.
    //
    // This is the method in your code where you would copy/duplicate
    // what is done here.
    
    if((self.urlTextField) && (self.urlTextField.text.length > 3))
    {
        [self.urlTextField resignFirstResponder];
        
        self.navigationItem.rightBarButtonItem = self.closeButtonItem;
        
        self.navigationItem.hidesBackButton = YES;
        
        // Build the web container view (if needed).
        [self buildWebContainerView];
        
        
        // Construct the URL (might load from a config file or plist).
        NSString *urlToLoadString = self.urlTextField.text;
        NSURL *url = [NSURL URLWithString:urlToLoadString];
        self.urlRequest = [NSMutableURLRequest requestWithURL:url];
        
        
        // DEV NOTE #3.
        //
        // This is the location where the cookie data is injected (if any).
        
        // Check to see if there is cookie data in the list. For return users, the
        // cookie data can be saved. It is loaded in the loadView method (above) or
        // it can be loaded from when it is convenient for your application.
        if((self.cookieDataArray) && (self.cookieDataArray.count > 0)) {
            
            // Build the cookie string from the list.
            NSMutableString *cookieBuildString = nil;
            
            for(NSString *cookieString in self.cookieDataArray) {
                if(cookieBuildString == nil)
                {
                    cookieBuildString = [NSMutableString stringWithString:cookieString];
                }
                else
                {
                    [cookieBuildString appendString:@";"];
                    [cookieBuildString appendString:cookieString];
                }
            }
            
            if(cookieBuildString) {
                self.cookieDataString = [NSString stringWithString:cookieBuildString];
                NSLog(@"INJECT String -> self.cookieDataString: %@", self.cookieDataString);
                
                [self saveCookieData];
            }
        }
        
        
        
        WKUserContentController *userContentController = nil;
        if(self.cookieDataString) {
            userContentController = [[WKUserContentController alloc] init];
            WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:self.cookieDataString
                                                                injectionTime:WKUserScriptInjectionTimeAtDocumentStart
                                                             forMainFrameOnly:NO];
            [userContentController addUserScript:cookieScript];
            [self.urlRequest addValue:self.cookieDataString forHTTPHeaderField:@"Cookie"];
        }
        else
        {
            // Skipping cookie injection because there are none to inject. This
            // will happen on the initial load.
        }
        
        
        
        
        if(self.webViewConfiguration == nil) {
            self.webViewConfiguration = [[WKWebViewConfiguration alloc] init];
        }
        
        
        if(userContentController) {
            self.webViewConfiguration.userContentController = userContentController;
        }
        
        
        
        if(self.vivoomWebView == nil) {
            self.vivoomWebView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:self.webViewConfiguration];
        }
        
        self.vivoomWebView.navigationDelegate = self;
        self.vivoomWebView.UIDelegate = self;
        self.vivoomWebView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.vivoomWebView loadRequest:urlRequest];
        [self.webContainerView addSubview:self.vivoomWebView];
        
        NSDictionary *viewDict = NSDictionaryOfVariableBindings(vivoomWebView);
        
        [self.webContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0.0-[vivoomWebView]-0.0-|" options:0 metrics:nil views:viewDict]];
        [self.webContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0.0-[vivoomWebView]-0.0-|" options:0 metrics:nil views:viewDict]];
    }
}


- (void)handleCloseButtonTouched:(id)sender
{
    [UIView animateWithDuration:0.44
                     animations:^ {
                         self.webContainerView.alpha = 0.0;
                         
                     } completion:^(BOOL finished) {
                         
                         [self.webContainerView removeFromSuperview];
                         self.navigationItem.rightBarButtonItem = nil;
                         
                         // Turn back on the "Back" button.
                         self.navigationItem.hidesBackButton = NO;
                     }];
}



- (void)saveCookieData
{
    if(self.cookieDataString) {
        [[NSUserDefaults standardUserDefaults] setObject:self.cookieDataString forKey:@"vivoom-campaign-cookie-data"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)loadCookieData
{
    if(self.cookieDataString) {
        self.cookieDataString = nil;
    }
    
    self.cookieDataString = [[NSUserDefaults standardUserDefaults] stringForKey:@"vivoom-campaign-cookie-data"];
}

#pragma mark - WKWebView


- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if(error)
    {
        NSLog(@"WKWebView -> didFailNavigation Error: %@", error.localizedDescription);
    }
}


- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    // DEV NOTE #2.
    //
    // This is the location where the cookie data is saved.
    if(self.urlRequest)
    {
        NSMutableURLRequest *request = [self.urlRequest mutableCopy];
        NSString *validDomain = request.URL.host;
        const BOOL requestIsSecure = [request.URL.scheme isEqualToString:@"https"];
        
        
        if(self.cookieDataArray == nil)
        {
            // This is the initial load of the page.
            self.cookieDataArray = [NSMutableArray array];
        }
        else
        {
            // This is a reload or revisit. Empty the list.
            [self.cookieDataArray removeAllObjects];
        }
        
        
        
        for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies])
        {
            // Don't even bother with values containing a `'`
            if ([cookie.name rangeOfString:@"'"].location != NSNotFound) {
                NSLog(@"Skipping %@ because it contains a '", cookie.properties);
                continue;
            }
            
            
            NSString *value = [NSString stringWithFormat:@"%@=%@", cookie.name, cookie.value];
            [self.cookieDataArray addObject:value];
            
            // Debug support
            //NSString *logString = [NSString stringWithFormat:@"Adding Cookie -> %@", value];
            //NSLog(@"%@", logString);
        }
        
        
        
        if(self.cookieDataArray.count > 0)
        {
            // Build the cookie string from the list.
            NSMutableString *cookieBuildString = nil;
            
            for(NSString *cookieString in self.cookieDataArray)
            {
                if(cookieBuildString == nil)
                {
                    cookieBuildString = [NSMutableString stringWithString:cookieString];
                }
                else
                {
                    [cookieBuildString appendString:@";"];
                    [cookieBuildString appendString:cookieString];
                }
            }
            
            if(cookieBuildString) {
                self.cookieDataString = [NSString stringWithString:cookieBuildString];
                NSLog(@"INJECT String -> self.cookieDataString: %@", self.cookieDataString);
                
                [self saveCookieData];
            }
        }
    }
}


- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    if (!navigationAction.targetFrame.isMainFrame) {
        UIApplication *app = [UIApplication sharedApplication];
        NSURL *url = navigationAction.request.URL;
        if([url.scheme isEqualToString:@"mailto"]) {
            if ([app canOpenURL:url]) {
                [app openURL:url];
                return nil;
            }
        }
        else if ([app canOpenURL:url]) {
            [app openURL:url];
            return nil;
        }
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    UIApplication *app = [UIApplication sharedApplication];
    NSURL *url = navigationAction.request.URL;
    if (!navigationAction.targetFrame) {
        if ([app canOpenURL:url]) {
            [app openURL:url];
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
    }
    if([url.scheme isEqualToString:@"mailto"]) {
        if ([app canOpenURL:url]) {
            [app openURL:url];
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}



- (void)buildWebContainerView
{
    self.webContainerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.webContainerView.alpha = 1.0;
    self.webContainerView.autoresizesSubviews = YES;
    self.webContainerView.backgroundColor = [UIColor darkGrayColor];
    self.webContainerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.webContainerView];
    
    NSDictionary *viewDict = NSDictionaryOfVariableBindings(webContainerView);
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0.0-[webContainerView]-0.0-|" options:0 metrics:nil views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0.0-[webContainerView]-0.0-|" options:0 metrics:nil views:viewDict]];
}



#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField * _Nonnull)textField
{
    return(YES);
}


- (void)textFieldDidBeginEditing:(UITextField * _Nonnull)textField
{
}


- (BOOL)textFieldShouldEndEditing:(UITextField * _Nonnull)textField
{
    return(YES);
}


- (void)textFieldDidEndEditing:(UITextField * _Nonnull)textField
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return(YES);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return(YES);
}



@end
