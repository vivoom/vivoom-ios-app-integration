# Background #

For background information about webapp integrations of Vivoom, please consult the top level README at ______________________

# README #

This README will document a sample iOS application which is meant to be a guide for using a WKWebView to launch a Vivoom campaign from your iOS application. Currently, the source code is compatible with iOS 9 and iOS 10. The application is written using Objective-C but since it serves as a guide, a Swift implementation should be straight forward.


### Version ###

* Version 1.0
* Xcode Version 8.1 (8T61a)




# Project Notes #


## Summary ##

This Xcode project will build a simple example iOS application that will display a view controller. The view controller will display a text field to manually enter a URL which can be triggered from a button. The button event handler will call a method which will present the WKWebView and display what the URL loads. This event handler will simulate a user touching a button in your app to start the Vivoom experience. For testing purposes please use the testing URL(s) Vivoom provides you.


In many cases when a WKWebView is used, a URL is provided and the web view will just run the HTML (web) code. However, this sample code will demonstrate that server cookie data can be received and saved. This user data can be locally saved and loaded for return visits. This sample application will show how that cookie data can be injected back into the WKWebView.
 

## Configuration ##

NOTE: The *bundle ID* and your *team information* will have to be updated in this project to run on any of your devices. 


* Your project/codebase will need WebKit included.
* Your project/codebase will need camera & mic permissions enabled.


## Integration Checklist ##

### 1) Open WebView ###

*Within code you can reference "DEV Note #1"
*

In the handleLaunchWebView method is the code to create and display the web view.

```
#!Objective-C

- (void)handleLaunchWebView:(id)sender
```

Please note, using the **SFSafariViewController** is not recommended. Currently, there is a known issue with iOS that prevents a web page loaded in the SFSafariViewController from obtaining the correct camera & mic permissions needed to capture video & audio. The Vivoom video and audio capture will not work until this issue is fixed by Apple in a future iOS release. *A bug report was filed with Apple, but marked as a duplicate issue and closed.*


### 2a) Cookie Saving/Persistence ###

*Within code you can reference "DEV Note #2"
*

The **didFinishNavigation** method contains the code to persist the cookie data. This allows your users to not need to re-enter any information. The **saveCookieData** method is an example saving to the NSUserDefaults. Feel free to save the data that is best for your app (CoreData, SQLite, serial archive, etc.).

```
#!Objective-C

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
```


### 2b) Cookie Loading/Injection ###

*Within code you can reference "DEV Note #3"
*

Also in the **handleLaunchWebView** method is the code that will check a local array list for cookie data. If there is any cookie data then it will add the cookies to the HTTP header field in the WKWebView request. 

The **loadCookieData** method is an example of how to load the cookie data from the NSUserDefaults. Use the method to load that is best for your app (which matches how you chose to save the data).


```
#!Objective-C

- (void)handleLaunchWebView:(id)sender
```